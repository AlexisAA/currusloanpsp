﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="CurrusLoan.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap-grid.css">
    <link rel="stylesheet" href="css/bootstrap-reboot.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">

    <title>MENU DE CARROS</title>

</head>

<body>
    
    
    <nav class="Navegation_Bar">
        <div class="img_logo">
            <img src="images/Logo.png" alt="Logo Currus Loan" srcset="" height="100px">
        </div>
        <div class="options">
            <ul>
                <li><a class="nav-link" href="">Inicio</a></li>
                <li><a class="nav-link" href="">Renta tu auto</a></li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Mi cuenta
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Ajustes</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
            </ul>
        </div>
        <form class="form-inline">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        </form>
    </nav>

    <br>

    <div class="container">
        <div class="row row-cols-1 row-cols-md-3">

            <div class="col mb-4">
                <div class="card h-100">
                    <img src="images/AveoTj1.jpeg" class="card-img-top" alt="AveoImg">
                    <div class="card-body">
                        <h5 class="card-title">Aveo 2011</h5>
                        <div class="card" style="width: 18rem;">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <ul class="list-group-item">
                                        <li>Estandar</li>
                                        <li>5 pasajeros</li>
                                        <li>4 puertas</li>
                                        <li>Vestiduras de tela</li>
                                        <li>39L</li>
                                    </ul>
                                </li>
                            </ul>
                            <li class="list-group-item">
                                <img class="img-profile" src="images/Caras/Tj1-Ar.jpg" alt="">
                                ROSA PAZOS JURADO

                            </li>
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary btn-lg">Ver más</button>
                </div>
            </div>

            <div class="col mb-4">
                <div class="card h-100">
                    <img src="images/SparkTj2.jpg" class="card-img-top " alt="AveoImg">
                    <div class="card-body">
                        <h5 class="card-title">Spark 2015</h5>
                        <div class="card" style="width: 18rem;">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <ul class="list-group-item">
                                        <li>Estandar</li>
                                        <li>4 pasajeros</li>
                                        <li>5 puertas</li>
                                        <li>Vestiduras de tela</li>
                                        <li>39L</li>
                                    </ul>
                                </li>
                            </ul>
                            <li class="list-group-item">
                                <img class="img-profile" src="images/Caras/Tj2-Ar.jpg" alt="">
                                LIDIA MURCIA CORREAS
                            </li>
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary btn-lg">Ver más</button>
                </div>
            </div>

            <div class="col mb-4">
                <div class="card h-100">
                    <img src="images/KiaRioTj3.jpg" class="card-img-top" alt="AveoImg">
                    <div class="card-body">
                        <h5 class="card-title">Rio 2018</h5>
                        <div class="card" style="width: 18rem;">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <ul class="list-group-item">
                                        <li>Estandar</li>
                                        <li>5 pasajeros</li>
                                        <li>4 puertas</li>
                                        <li>Vestiduras de tela</li>
                                        <li>45L</li>
                                    </ul>
                                </li>
                            </ul>
                            <li class="list-group-item">
                                <img class="img-profile" src="images/Caras/Tj3-Ar.jpg" alt="">
                                AURORA CURTO CLAVERIA
                            </li>
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary btn-lg">Ver más</button>
                </div>
            </div>

            <div class="col mb-4">
                <div class="card h-100">
                    <img src="images/JettaTj4.jpg" class="card-img-top" alt="AveoImg">
                    <div class="card-body">
                        <h5 class="card-title">Jetta 2017</h5>
                        <div class="card" style="width: 18rem;">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <ul class="list-group-item">
                                        <li>Estandar</li>
                                        <li>5 pasajeros</li>
                                        <li>4 puertas</li>
                                        <li>Vestiduras de piel</li>
                                        <li>35L</li>
                                    </ul>
                                </li>
                            </ul>
                            <li class="list-group-item">
                                <img class="img-profile" src="images/Caras/Tj4-Ar.jpg" alt="">
                                BRUNO MARTINEZ
                            </li>
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary btn-lg">Ver más</button>
                </div>
            </div>

            <div class="col mb-4">
                <div class="card h-100">
                    <img src="images/GolfTj5.jpg" class="card-img-top" alt="AveoImg">
                    <div class="card-body">
                        <h5 class="card-title">Golf 2016</h5>
                        <div class="card" style="width: 18rem;">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <ul class="list-group-item">
                                        <li>Automatico</li>
                                        <li>5 pasajeros</li>
                                        <li>4 puertas</li>
                                        <li>Vestiduras de piel</li>
                                        <li>50L</li>
                                    </ul>
                                </li>
                            </ul>
                            <li class="list-group-item">
                                <img class="img-profile" src="images/Caras/Tj5-Ar.jpg" alt="">
                                TIJS MARCOS BARBA
                            </li>
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary btn-lg">Ver más</button>
                </div>                
            </div>

        </div> <!--row row-cols-1 row-cols-md-3-->
    </div> <!--container-->

    <script src="js/scrip.js"></script>
    <script src="js/bootstrap.bundle.js"></script>
    <script src="js/bootstrap.js"></script>


</body>
</html>


